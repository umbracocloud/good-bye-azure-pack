﻿using System;

namespace GoodByeAzurePack.Core.SocketsSignal
{
	public struct GraphDataHash
	{
		private string value { get; set; }

		public GraphDataHash(string value)
		{
			this.value = value;
		}

		public override bool Equals(object obj)
		{
			if (obj is GraphDataHash anyProductPartNumber)
			{
				return this.Equals(anyProductPartNumber);
			}

			return false;
		}

		public bool Equals(GraphDataHash other)
		{
			return this.value.Equals(other.value, StringComparison.InvariantCulture);
		}

		public override int GetHashCode()
		{
			return this.value?.GetHashCode(StringComparison.Ordinal) ?? 0;
		}

		public override string ToString()
		{
			return this.value;
		}

		public static implicit operator GraphDataHash(string value)
		{
			return new GraphDataHash(value);
		}

		public static explicit operator string(GraphDataHash graphDataHash)
		{
			return graphDataHash.value;
		}

		public static bool operator ==(GraphDataHash left, GraphDataHash right)
		{
			return left.value == right.value;
		}

		public static bool operator !=(GraphDataHash left, GraphDataHash right)
		{
			return !(left == right);
		}
	}
}