﻿using System;

namespace GoodByeAzurePack.Core.SocketsSignal
{
	public class GraphData
	{
		public string Response { get; init; }

		public DateTime FromDate { get; set; }
		public DateTime ToDate { get; set; }
	}
}