﻿namespace GoodByeAzurePack.Core.APIStuff
{
	public class FunnelItemDTO
	{
		public string Label { get; set; }
		public int Total { get; set; }
	}
}
