﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.ViewEngines;
using Microsoft.Extensions.Logging;
using Umbraco.Cms.Core.Web;
using Umbraco.Cms.Web.Common.Controllers;

namespace GoodByeAzurePack.Web
{

    public class DashboardController : RenderController
	{
        private ILogger<DashboardController> _logger;

        public DashboardController(ILogger<DashboardController> logger,
			ICompositeViewEngine compositeViewEngine,
			IUmbracoContextAccessor umbracoContextAccessor)
			: base(logger, compositeViewEngine, umbracoContextAccessor)
		{
			_logger = logger;
		}
		public override IActionResult Index()
		{
			_logger.LogInformation("Dashboard request!!");
			return CurrentTemplate(CurrentPage);
		}
	}

}
