﻿"use strict";
var connection = new signalR.HubConnectionBuilder().withUrl("/dashboardHub").build();
var createDataStructure = function (theCurrentValue) {
	var myData = JSON.parse(atob(theCurrentValue));
	return {
		labels: myData.map(function (item) { return item.Label }),
		colors: ['#FFB178', '#FF3C8E'],
		values: myData.map(function (item) { return item.Total })
	};
}

var graph;
var confettiSettings = { target: 'confetti-canvas', height: 100, respawn: false };
var confetti = new ConfettiGenerator(confettiSettings);
connection.on("OnVersionChange", function (message) {
	if (!graph) {
		graph = new FunnelGraph({
			container: '.funnel-container',
			gradientDirection: 'horizontal',
			data: createDataStructure(message),
			direction: 'horizontal',
			displayPercent: false
		});
		graph.draw();
		
	} else {
		confetti.render();
		var data = createDataStructure(message);
		//calling it twice because of a bug in the library
		graph.updateData(data); graph.updateData(data);
	}
});

async function start() {
	try {
		await connection.start();
	} catch (err) {
		console.log(err);
		setTimeout(start, 5000);
	}
};

connection.onclose(async () => {
	await start();
});

// Start the connection.
start();

